<?php
/**
 * Ce fichier contient l'ensemble des constantes et fonctions de construction du contenu des réponses aux
 * requête à l'API SVP.
 *
 * @package SPIP\SVPAPI\PLUGIN
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_SVPAPI_CONFIG_CHAMPS')) {
	/**
	 * Liste des champs des objets plugin ou paquet contenue dans la réponse à l'API et type de normalisation
	 * à effectuer sur chaque champ le cas échéant.
	 */
	define(
		'_SVPAPI_CONFIG_CHAMPS',
		[
			'plugin' => [
				// SVP référentiel par défaut
				'prefixe'            => ['svp' => true, 'type' => ''],
				'nom'                => ['svp' => true, 'type' => 'multi'],
				'slogan'             => ['svp' => true, 'type' => 'multi'],
				'vmax'               => ['svp' => true, 'type' => 'version'],
				'date_crea'          => ['svp' => false, 'type' => ''],
				'date_modif'         => ['svp' => false, 'type' => ''],
				'compatibilite_spip' => ['svp' => true, 'type' => ''],
				'branches_spip'      => ['svp' => true, 'type' => 'liste'],
				// SVP Statistiques
				'nbr_sites'  => ['svp' => false, 'type' => ''],
				'popularite' => ['svp' => false, 'type' => ''],
			],
			'paquet' => [
				// SVP référentiel par défaut
				'prefixe'            => ['svp' => true, 'type' => ''],
				'logo'               => ['svp' => true, 'type' => ''],
				'version'            => ['svp' => true, 'type' => 'version'],
				'version_base'       => ['svp' => true, 'type' => 'version'],
				'compatibilite_spip' => ['svp' => true, 'type' => ''],
				'branches_spip'      => ['svp' => true, 'type' => 'liste'],
				'description'        => ['svp' => true, 'type' => 'multi'],
				'auteur'             => ['svp' => true, 'type' => 'serial'],
				'credit'             => ['svp' => false, 'type' => 'serial'],
				'licence'            => ['svp' => false, 'type' => 'serial'],
				'copyright'          => ['svp' => false, 'type' => 'serial'],
				'lien_doc'           => ['svp' => true, 'type' => ''],
				'lien_demo'          => ['svp' => true, 'type' => ''],
				'lien_dev'           => ['svp' => false, 'type' => ''],
				'etat'               => ['svp' => true, 'type' => ''],
				'etatnum'            => ['svp' => true, 'type' => ''],
				'dependances'        => ['svp' => false, 'type' => 'serial'],
				'procure'            => ['svp' => false, 'type' => 'serial'],
				'date_crea'          => ['svp' => false, 'type' => ''],
				'date_modif'         => ['svp' => false, 'type' => ''],
				'nom_archive'        => ['svp' => true, 'type' => ''],
				'nbo_archive'        => ['svp' => true, 'type' => ''],
				'maj_archive'        => ['svp' => true, 'type' => ''],
				'src_archive'        => ['svp' => true, 'type' => ''],
				'traductions'        => ['svp' => false, 'type' => 'serial'],
			]
		]
	);
}

/**
 * Transforme, pour un objet plugin ou paquet, les champs sérialisés, multi et liste (chaine d'éléments séparés
 * par une virgule) en tableau et supprime des champs de type version les 0 à gauche des numéros.
 *
 * @uses normaliser_multi()
 * @uses denormaliser_version()
 *
 * @param string $type_objet Type d'objet à normaliser, soit `plugin` ou `paquet`.
 * @param array  $objet      Tableau des champs de l'objet `plugin` ou `paquet` à normaliser.
 *
 * @return array Tableau des champs de l'objet `plugin` ou `paquet` normalisés.
 */
function plugin_normaliser_champs(string $type_objet, array $objet) : array {
	// On initialise l'objet normalisé à vide pour éventuellement filtrer les champs inutiles et
	// renvoyer vide si erreur.
	$objet_normalise = [];

	// Extraire la liste des champs du type d'objet : on prend tout sans se soucier du fait que ce
	// soit dans le cadre d'utilisation de SVP ou autre.
	// -- on utilise une statique car cette fonction est appelée sur chaque objet
	static $champs_config = [];
	if (!isset($champs_config[$type_objet])) {
		$options = [
			'pour_svp'  => false,
			'avec_type' => true
		];
		$champs_config[$type_objet] = plugin_lister_champs($type_objet, $options);
	}

	if (
		$objet
		and $champs_config[$type_objet]
	) {
		// On boucle sur les champs de l'objet pour les normaliser au besoin selon leur type.
		include_spip('plugins/preparer_sql_plugin');
		include_spip('svp_fonctions');
		foreach ($objet as $_champ => $_valeur) {
			if (isset($champs_config[$type_objet][$_champ]['type'])) {
				$type = $champs_config[$type_objet][$_champ]['type'];
				if ($type) {
					// On traite les normalisations
					if ($type === 'multi') {
						// Passer un champ multi en tableau indexé par la langue
						$objet_normalise[$_champ] = normaliser_multi($_valeur);
					} elseif ($type === 'serial') {
						// Désérialiser un champ sérialisé
						$objet_normalise[$_champ] = unserialize($_valeur);
					} elseif ($type === 'version') {
						// Retourne la chaine de la version x.y.z sous sa forme initiale, sans
						// remplissage à gauche avec des 0.
						$objet_normalise[$_champ] = denormaliser_version($_valeur);
					} elseif ($type === 'liste') {
						// Passer une chaine liste en tableau
						$objet_normalise[$_champ] = $_valeur ? explode(',', $_valeur) : [];
					}
				} else {
					// Par défaut on initialise le champ à sa valeur non normalisée
					$objet_normalise[$_champ] = $_valeur;
				}
			}
		}
	}

	return $objet_normalise;
}

/**
 * Renvoie la liste des champs à renvoyer suite à une requête sur les collections paquets ou plugins.
 *
 * @param string     $type_objet Type d'objet à normaliser, soit `plugin` ou `paquet`.
 * @param null|array $options    Permet d'indiquer si la demande est pour SVP (gestion de l'installation des plugins)
 *                               et le format de la liste souhaitée:
 *                               - pour_svp  : true/false (par défaut true)
 *                               - avec_type : true, valeur par défaut, renvoie un tableau [champ] = type, false renvoie
 *                               juste la liste des champs.
 *
 * @return array Liste des champs de l'objet `plugin` ou `paquet` utiles.
 */
function plugin_lister_champs(string $type_objet, ?array $options = []) {
	// Par défaut, si erreur d'argument on renvoie un tableau vide
	$champs = [];

	if (isset(_SVPAPI_CONFIG_CHAMPS[$type_objet])) {
		// Initialisation des options
		if (!isset($options['pour_svp'])) {
			$options['pour_svp'] = true;
		}
		if (!isset($options['pour_svp'])) {
			$options['avec_type'] = true;
		}

		// Extraction des champs nécessaires et gestion des champs supplémentaires de SVP Stats
		$champs_objet = _SVPAPI_CONFIG_CHAMPS[$type_objet];
		if (
			($type_objet === 'plugin')
			and !defined('_DIR_PLUGIN_SVPSTATS')
		) {
			// On supprime nbr_sites et popularite
			unset($champs_objet['nbr_sites'], $champs_objet['popularite']);
		}

		if ($options['pour_svp']) {
			// Uniquement les champs nécessaire à SVP
			$champs_objet = array_filter($champs_objet, function ($v) {
				return !empty($v['svp']);
			});
		}

		// Mise en liste de champs simple si demandé
		$champs = $options['avec_type'] ? $champs_objet : array_keys($champs_objet);
	}

	return $champs;
}
