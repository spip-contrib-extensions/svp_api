<?php
/**
 * Ce fichier contient l'ensemble des fonctions de service spécifiques à une ou plusieurs collections.
 *
 * @package SPIP\SVPAPI\SERVICE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// -----------------------------------------------------------------------
// ---------------------------- CONTEXTE ---------------------------------
// -----------------------------------------------------------------------
/**
 * Détermine si le serveur est capable de répondre aux requêtes SVP.
 * Pour cela on vérifie si le serveur est en mode run-time ou pas.
 * On considère qu'un serveur en mode run-time n'est pas valide pour
 * traiter les requêtes car la liste des plugins et des paquets n'est
 * pas complète.
 *
 * @param array<string, mixed> &$erreur Tableau initialisé avec les index identifiant l'erreur ou vide si pas d'erreur.
 *                                      Les index mis à jour sont uniquement les suivants car les autres sont initialisés par l'appelant :
 *                                      - `type`    : identifiant de l'erreur 501, soit `runtime_nok`
 *                                      - `element` : type d'objet sur lequel porte l'erreur, soit `serveur`
 *                                      - `valeur`  : la valeur du mode runtime
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function svpapi_api_verifier_contexte(array &$erreur) : bool {
	// Initialise le retour à true par défaut.
	$est_valide = true;

	include_spip('inc/svp_phraser');
	include_spip('inc/config');
	$mode = lire_config('svp/mode_runtime', 'non');
	if (_SVP_MODE_RUNTIME
	or (!_SVP_MODE_RUNTIME and ($mode == 'oui'))) {
		$erreur['type'] = 'runtime_nok';
		$erreur['element'] = 'svp_mode_runtime';
		$erreur['valeur'] = _SVP_MODE_RUNTIME;

		$est_valide = false;
	}

	return $est_valide;
}

/**
 * Compléte le bloc d'information du plugin en supprimant le schéma du plugin SVP API qui n'existe pas par celui
 * du plugin SVP sur lequel s'appuie SVP API.
 *
 * @param array $contenu Le contenu de la réponse dans son état après initialisation.
 *
 * @return array Le contenu de la réponse avec l'index `schema` supprimé et remplacé par l'index `schema_svp`.
 */
function svpapi_reponse_informer_plugin(array $contenu) : array {
	// On met à jour les informations sur le plugin utilisateur maintenant qu'il est connu.
	// -- Récupération du schéma de données et de la version du plugin.
	include_spip('inc/filtres');
	$informer = charger_filtre('info_plugin');
	$schema = $informer('svp', 'schema', true);

	$contenu['fournisseur']['schema'] = "{$schema} (SVP)";

	return $contenu;
}

// -----------------------------------------------------------------------
// ----------------------------- PLUGINS ---------------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des plugins de la table spip_plugins éventuellement filtrés par les critères
 * additionnels positionnés dans la requête.
 * Les plugins fournis sont toujours issus d'un dépôt hébergé par le serveur ce qui exclu les plugins
 * installés sur le serveur et non liés à un dépôt (par exemple un zip personnel).
 * Chaque objet plugin est présenté comme un tableau dont tous les champs sont accessibles comme un
 * type PHP simple, entier, chaine ou tableau.
 *
 * @uses plugin_normaliser_champs()
 *
 * @param array                $conditions    Tableau des conditions SQL à appliquer au select et correspondant aux filtres
 *                                            passés dans la requête.
 * @param array<string, mixed> $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array<string, mixed> $configuration Configuration de la collection.
 *
 * @return array Tableau des plugins dont l'index est le préfixe du plugin.
 *               Les champs de type id ou maj ne sont pas renvoyés.
 */
function plugins_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la collection
	$plugins = [];

	// Récupérer la liste des plugins (filtrée ou pas).
	// -- Les plugins appartiennent forcément à un dépot logique installés sur le serveur. Les plugins
	//    installés directement sur le serveur, donc hors dépôt sont exclus.
	$from = ['spip_plugins', 'spip_depots_plugins'];
	$group_by = ['spip_plugins.id_plugin'];
	// -- Tous le champs sauf id_plugin et id_depot.
	include_spip('inc/svpapi_plugin');
	$options = [
		'pour_svp'  => isset($filtres['type_reponse']) and ($filtres['type_reponse'] === 'svp'),
		'avec_type' => false
	];
	$select = plugin_lister_champs('plugin', $options);

	// -- Initialisation du where avec les conditions sur la table des dépots.
	$where = [
		'spip_depots_plugins.id_depot>0',
		'spip_depots_plugins.id_plugin=spip_plugins.id_plugin'
	];
	// -- Si il y a des critères additionnels on complète le where en conséquence en fonction de la configuration.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}

	// Chargement de la collection demandée
	$collection = sql_allfetsel($select, $from, $where, $group_by);

	// On refactore le tableau de sortie du allfetsel en un tableau associatif indexé par les préfixes.
	// On transforme les champs multi en tableau associatif indexé par la langue et on désérialise les
	// champs sérialisés.
	if ($collection) {
		foreach ($collection as $_plugin) {
			$plugins[$_plugin['prefixe']] = plugin_normaliser_champs('plugin', $_plugin);
		}
	}

	return $plugins;
}

/**
 * Retourne la description complète d'un plugin et de ses paquets.
 *
 * @param string               $prefixe       La valeur du préfixe du plugin.
 * @param array<string, mixed> $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array<string, mixed> $configuration Configuration de la collection.
 *
 * @return array La description du plugin et de ses paquets, les champs étant tous normalisés (désérialisés).
 */
function plugins_ressourcer(string $prefixe, array $filtres, array $configuration) : array {
	// Initialisation du tableau de la ressource
	$ressource = [];

	// On recherche d'abord le plugin par son préfixe dans la table spip_plugins.
	// -- Liste des champs utiles
	include_spip('inc/svpapi_plugin');
	$options = [
		'pour_svp'  => isset($filtres['type_reponse']) and ($filtres['type_reponse'] === 'svp'),
		'avec_type' => false
	];
	$select = plugin_lister_champs('plugin', $options);
	// -- Acquisition du plugin (on est sur qu'il est en base) et suppression de l'id qui est inutile.
	include_spip('inc/svp_plugin');
	$plugin = plugin_lire($prefixe, $select);
	// -- Normalisation des champs.
	$ressource['plugin'] = plugin_normaliser_champs('plugin', $plugin);

	// On recherche maintenant les paquets du plugin.
	$from = ['spip_paquets'];

	// -- Tous le champs sauf id_plugin et id_depot (même options que pour les plugins).
	$select = plugin_lister_champs('paquet', $options);

	// -- Préfixe et conditions sur le dépôt pour exclure les paquets installés.
	$where = [
		'prefixe=' . sql_quote(strtoupper($prefixe)),
		'id_depot>0'
	];

	// Acquisition des paquets et normalisation des champs.
	include_spip('svp_fonctions');
	$ressource['paquets'] = [];
	$paquets = sql_allfetsel($select, $from, $where);
	if ($paquets) {
		// On refactore en un tableau associatif indexé par archives zip.
		foreach ($paquets as $_paquet) {
			$ressource['paquets'][denormaliser_version($_paquet['version'])] = plugin_normaliser_champs('paquet', $_paquet);
		}
	}

	return $ressource;
}

/**
 * Construit la condition SQL inhérente au critère de filtre `compatible_spip`.
 *
 * @param mixed $version La valeur du critère compatibilite SPIP : une version, une branche ou une liste de branches séparées par
 *                       des virgules.
 *
 * @return string Condition SQL du filtre.
 */
function plugins_conditionner_compatible_spip($version) : string {
	$filtrer = charger_fonction('where_compatible_spip', 'inc');
	$condition = $filtrer($version, 'spip_plugins', '>');

	return $condition;
}

/**
 * Détermine si la valeur du critère compatibilité SPIP est valide.
 * La fonction compare uniquement la structure de la chaine passée qui doit être cohérente avec
 * un numéro de version ou de branche.
 *
 * @param mixed                $valeur  La valeur du critère compatibilite SPIP
 * @param array<string, mixed> &$erreur Tableau initialisé avec les index identifiant l'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function plugins_verifier_filtre_compatible_spip($valeur, array &$erreur) : bool {
	$est_valide = true;

	if (!preg_match('#^((?:\d+)(?:\.\d+){0,2})(?:,(\d+\.\d+)){0,}$#', $valeur)) {
		$est_valide = false;
		$erreur['type'] = 'critere_compatible_spip_nok';
	}

	return $est_valide;
}

/**
 * Détermine si la valeur du préfixe de plugin est valide.
 * La fonction compare uniquement la structure de la chaine passée qui doit être cohérente avec
 * celui d'un nom de variable.
 *
 * @param string               $prefixe La valeur du préfixe
 * @param array<string, mixed> &$erreur Tableau initialisé avec les index identifiant l'erreur.
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function plugins_verifier_ressource_prefixe(string $prefixe, array &$erreur) : bool {
	$est_valide = true;

	// On teste en premier si le préfixe est syntaxiquement correct pour éviter un accès SQL dans ce cas.
	if ((int) $prefixe or !preg_match('#^(\w){2,}$#', strtolower($prefixe))) {
		$est_valide = false;
		$erreur['type'] = 'prefixe_malforme';
	} else {
		// On vérifie ensuite si la ressource est bien un plugin fourni par un dépôt
		// et pas un plugin installé sur le serveur uniquement.
		include_spip('inc/svp_plugin');
		if (!plugin_lire($prefixe)) {
			$est_valide = false;
			$erreur['type'] = 'prefixe_nok';
		}
	}

	return $est_valide;
}

// -----------------------------------------------------------------------
// ----------------------------- PAQUETS ---------------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des paquets hébergés par le serveur.
 * C'est la requête de base pour l'installation des plugins par SVP.
 *
 * @param array                $conditions    Tableau des conditions SQL à appliquer au select et correspondant aux filtres
 *                                            passés dans la requête.
 * @param array<string, mixed> $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array<string, mixed> $configuration Configuration de la collection.
 *
 * @return array Tableau des paquets. Les champs de type id ou maj ne sont pas renvoyés.
 */
function paquets_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Initialisation de la sortie.
	$paquets = [];

	// Récupérer la liste des paquets à minima filtrée sue la branche spip.
	$from = ['spip_paquets'];
	// -- Seuls les champs signifiants pour SVP
	include_spip('inc/svpapi_plugin');
	$options = [
		'pour_svp'  => isset($filtres['type_reponse']) and ($filtres['type_reponse'] === 'svp'),
		'avec_type' => false
	];
	$select = plugin_lister_champs('paquet', $options);

	// -- Initialisation du where avec les conditions sur la table des dépots.
	$where = ['id_depot>0'];
	// -- Si il y a des critères additionnels on complète le where en conséquence en fonction de la configuration.
	if ($conditions) {
		$where = array_merge($where, $conditions);
	}

	$collection = sql_allfetsel($select, $from, $where);

	// On recherche maintenant la description du ou des plugins correspondant aux paquets récupérés.
	if ($collection) {
		// Si le filtre préfixe est utilisé c'est que l'on veut uniquement un seul plugin, inutile d'appeler la collection
		// plugins entière
		if (!empty($filtres['prefixe'])) {
			// -- Liste des champs
			$select = plugin_lister_champs('plugin', $options);
			// -- Récupération des champs, le plugin existe forcément
			include_spip('inc/svp_plugin');
			$plugin = plugin_lire($filtres['prefixe'], $select);
			// On crée le tableau comme pour le cas sans préfixe
			$plugins[strtoupper($filtres['prefixe'])] = plugin_normaliser_champs('plugin', $plugin);
		} else {
			// -- Acquérir la configuration de la collection plugins.
			include_spip('ezrest/ezrest');
			$configuration_plugins = configuration_ezrest_lire('plugins');
			// -- Extraire des filtres de la collection paquets ceux qui sont compatibles pour la collection plugins.
			$criteres = array_keys(array_column($configuration_plugins['filtres'], null, 'critere'));
			$filtres_plugins = [];
			foreach ($filtres as $_critere => $_valeur) {
				if (in_array($_critere, $criteres)) {
					$filtres_plugins[$_critere] = $_valeur;
				}
			}
			// Calculer les conditions associées aux filtres résiduels
			$conditions_plugins = ezrest_conditionner(
				'svpapi',
				'plugins',
				$filtres_plugins,
				$configuration_plugins
			);

			// -- acquérir la liste des paquets en réutilisant les fonctions de la collection plugins
			$plugins = plugins_collectionner($conditions_plugins, $filtres_plugins, $configuration_plugins);
		}

		// On présente la liste des paquets par plugin, avec la description de chaque plugin.
		// La liste des paquets d'un plugin est accessible dans un index 'paquets'.
		include_spip('svp_fonctions');
		foreach ($collection as $_paquet) {
			if (isset($plugins[$_paquet['prefixe']])) {
				// On a bien un plugin pour le paquet
				if (!isset($paquets[$_paquet['prefixe']]['paquets'])) {
					// C'est le premier paquet du plugin : on ajoute la description du plugin qui est déjà normalisée
					$paquets[$_paquet['prefixe']] = $plugins[$_paquet['prefixe']];
				}
				// On ajoute le paquet
				$paquets[$_paquet['prefixe']]['paquets'][denormaliser_version($_paquet['version'])] = plugin_normaliser_champs('paquet', $_paquet);
			}
		}

		// Ranger les paquets par ordre alphabétique des préfixes
		ksort($paquets);
	}

	return $paquets;
}

/**
 * Construit la condition SQL inhérente au critère de filtre `compatible_spip`.
 *
 * @param mixed $version La valeur du critère compatibilite SPIP : une version, une branche ou une liste de branches séparées par
 *                       des virgules.
 *
 * @return string Condition SQL du filtre.
 */
function paquets_conditionner_compatible_spip($version) : string {
	$filtrer = charger_fonction('where_compatible_spip', 'inc');
	$condition = $filtrer($version, 'spip_paquets', '>');

	return $condition;
}

// -----------------------------------------------------------------------
// ----------------------------- DEPOTS ----------------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste des dépôts hébergés par le serveur.
 * Contrairement aux plugins et paquets les champs d'un dépôt ne nécessitent aucun formatage.
 *
 * @param array                $conditions    Tableau des conditions SQL à appliquer au select et correspondant aux filtres
 *                                            passés dans la requête.
 * @param array<string, mixed> $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array<string, mixed> $configuration Configuration de la collection.
 *
 * @return array Tableau des dépôts. Les champs de type id ou maj ne sont pas renvoyés.
 */
function depots_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Récupérer la liste des dépôts (filtrée ou pas).
	include_spip('base/objets');
	$from = ['spip_depots'];
	// -- Tous le champs sauf maj et id_depot.
	$description_table = lister_tables_objets_sql('spip_depots');
	$select = array_keys($description_table['field']);
	$select = array_diff($select, ['id_depot', 'maj']);

	// -- Initialisation du where avec les conditions sur la table des dépots.
	$where = [];
	// -- Si il y a des critères additionnels on complète le where en conséquence.
	if ($conditions) {
		$where = $conditions;
	}

	$depots = sql_allfetsel($select, $from, $where);

	return $depots;
}
